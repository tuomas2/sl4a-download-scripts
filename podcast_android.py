#-*- encoding: utf-8 -*-
#quick (and dirty) script to download some W. L. Craig Defenders podcast mp3s straight to my android phone

import android
import urllib
import os
import re
import sys

#config starts
skipfromstart = 137 #defenders 2 beginning
howmany = 14# how many we want to download at once?
defaultdir = "/extSdCard/Download/defenders_podcast/" #%s"%pathmatch.match(defurl).groups()[0]
rbfile="/sdcard/rockbox/most-recent.bmark"
defaultfeed = "http://feeds.feedburner.com/DefendersPodcast"
#config ends

droid = android.Android()
droid.wakeLockAcquirePartial()

def peruuta():                             
    droid.makeToast("Cancelled")
    droid.wakeLockRelease()
    sys.exit(0)       

result = droid.dialogGetInput("URL", "Give URL or cancel -> barcode!", defaultfeed)

if result[1] == None:
    barcode = droid.scanBarcode()
    defurl = barcode[1]["extras"]["SCAN_RESULT"]
else:
    defurl = result[1]

pathmatch = re.compile(".*\/([^\/]*).*")


result = droid.dialogGetInput("PATH", "Where do you want to download these?",defaultdir)
if result[1] == None: peruuta()
path = result[1]

result = droid.dialogGetInput("Number", "How many you want to download?",str(howmany))
if result[1] == None: peruuta()
howmany = int(result[1])

try:
    os.mkdir(path)
except OSError:
    pass #file exists, we are fine

lines = urllib.urlopen(defurl).read().split("\n")#<item>")

def tprint(*ss):
    s = ""
    for si in ss:
        s += str(si) + " "
    droid.makeToast(s)
    print s 

feedmatch = re.compile(r".*media:content url=\"(http[^\"]*\.mp3)\".*")
filematch = re.compile(".*\/([^\/]*\.mp3).*")
count = -1
flist = []
for l in lines:
    m = feedmatch.match(l)
    if m:
        url = m.groups()[0]
        filename = filematch.match(url).groups()[0]
        flist.append((url,filename))
flist.reverse()

todaycount = 0

count1 = -1
lastok = -1
#log = open(path+"/log.txt","w")
for url,filename in flist:
    #seeking last ok file to continue from there
    count1 += 1
    wfilename = "%.4d_%s"%(count1,filename)
    nfilename = path+"/"+wfilename
#    log.write(wfilename+"\n")
    if os.path.exists(nfilename) and os.stat(nfilename).st_size!=0:
        lastok = count
#log.close()
count = lastok
if skipfromstart > lastok:
    count = skipfromstart-1

flist = flist[count+1:]

for url,filename in flist:
    count += 1
    wfilename = "%.4d_%s"%(count,filename)
    nfilename = path+"/"+wfilename
        
    if os.path.exists(nfilename) and os.stat(nfilename).st_size!=0:
        #tprint("Skipping",nfilename)
        continue

    if count == 0:
        bmark=">3;0;0;0;0;0;0;10000;10000;%s;%s\n"%(path,wfilename)
        tprint("Adding rockbox bookmark for",wfilename)
        infile=open(rbfile,"r")
        oldentries=infile.read()
        infile.close()
        of=open(rbfile,"w")
        of.write(bmark)
        of.write(oldentries)
        of.close()

    tprint("Downloading",count+1,"of",len(flist),":",filename)
    def kill_handler():
        tprint("Removing",nfilename)
        os.unlink(nfilename)
        droid.wakeLockRelease()
        sys.exit(0)
    import signal
    signal.signal(signal.SIGTERM, kill_handler)
    try:
        open(nfilename,"w").write(urllib.urlopen(url).read())
    except KeyboardInterrupt:
        kill_handler()
    todaycount += 1
    if todaycount >=howmany:
        break
tprint("Podcast downloads ready!")
droid.wakeLockRelease()

