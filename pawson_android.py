#-*- encoding: utf-8 -*-
import android
import urllib
import os
import re
import sys

#config alkaa
rbfile="/sdcard/rockbox/most-recent.bmark"
basedir = "/extSdCard/Download/pawson/"
#config päättyy

droid = android.Android()
droid.wakeLockAcquirePartial()
def tprint(*ss):
    s = ""
    for si in ss:
        s += str(si) + " "
    droid.makeToast(s)
    print s 

def peruuta():                             
    tprint("Peruutettu")
    droid.wakeLockRelease()
    sys.exit(0)       


result = droid.dialogGetInput("URL", "Anna url tai Peruuta -> barcode!","")

if result[1] == None:
    barcode = droid.scanBarcode()
    defurl = barcode[1]["extras"]["SCAN_RESULT"]
else:
    defurl = result[1]

#if result[1] == None:
#    peruuta()

#defurl = result[1]

pathmatch = re.compile(".*\/([^\/]*).*")
try: 
    defaultdir = basedir+pathmatch.match(defurl).groups()[0]
except:
    tprint("Major probleem...")
    sys.exit(0)

result = droid.dialogGetInput("PATH", "Minne ladataan?",defaultdir)

if result[1] == None:
    peruuta()

path = result[1]

try:
    os.mkdir(path)
except OSError:
    pass #file exists, we are fine

#"http://feeds.feedburner.com/DefendersPodcast"
lines = urllib.urlopen(defurl).read().split("<a")


feedmatch = re.compile(r".*href=\"(http[^\"]*\.mp3)\".*")
filematch = re.compile(".*\/([^\/]*\.mp3).*")
count = -1
flist = []
for l in lines:
    m = feedmatch.match(l)
    if m:
        url = m.groups()[0]
        filename = filematch.match(url).groups()[0]
        flist.append((url,filename))
#flist.reverse()
for url,filename in flist:
    count += 1
    wfilename = "%.4d_%s"%(count,filename)
    nfilename = path+"/"+wfilename
        
    if os.path.exists(nfilename) and os.stat(nfilename).st_size!=0:
        #tprint("Skipping",nfilename)
        continue

    if count == 0:
        bmark=">3;0;0;0;0;0;0;10000;10000;%s;%s\n"%(path,wfilename)
        tprintf("Adding rockbox bookmark for",wfilename)
        infile=open(rbfile,"r")
        oldentries=infile.read()
        infile.close()
        of=open(rbfile,"w")
        of.write(bmark)
        of.write(oldentries)
        of.close()

    tprint("Downloading",count+1,"of",len(flist),":",filename)
    def kill_handler():
        tprint("Removing",nfilename)
        os.unlink(nfilename)
        droid.wakeLockRelease()
        sys.exit(0)
    import signal
    signal.signal(signal.SIGTERM, kill_handler)
    #signal.signal(signal.SIGKILL, kill_handler)
    try:
        open(nfilename,"w").write(urllib.urlopen(url).read())
    except KeyboardInterrupt:
        kill_handler()

tprint("Pawson downloads ready!")
droid.wakeLockRelease()

