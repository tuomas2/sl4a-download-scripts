Pawson & W.L.Craig download for android (py4a)
==============================================

Python-for-android scripts for downloading mp3s for Android phones.
Adds also rockbox bookmark file for pawsons.

Warning: rather quick & dirty Python scripts. For advanced users only.

pawson_android.py: download Pawson's sermons from davidpawson.org 
    for your android device
podcast_android.py: download podcast mp3s (from Defenders podcast, 
    William Lane Craig, for your Android device)

Both scripts require sl4a and py4a installed in an Android phone:
<http://code.google.com/p/python-for-android/>
<http://code.google.com/p/android-scripting/>
